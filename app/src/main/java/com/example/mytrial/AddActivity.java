package com.example.mytrial;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mytrial.helper.SqliteHelper;
import com.google.android.material.snackbar.Snackbar;

public class AddActivity extends AppCompatActivity {

    // buat variable form
    RadioGroup radio_status;
    EditText edit_jumlah, edit_keterangan;
    Button btn_save;
    String status = "";

    //variable sqlite
    SqliteHelper sqliteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Add");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //variable sqlite
        sqliteHelper = new SqliteHelper(this);

        // deklarasi variable form

        radio_status = (RadioGroup) findViewById(R.id.radio_status);
        edit_jumlah= (EditText) findViewById(R.id.edit_jumlah);
        edit_keterangan = (EditText) findViewById(R.id.edit_keterangan);
        btn_save = (Button) findViewById(R.id.btn_save);

        radio_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if(i == R.id.radio_masuk){
                        status = "MASUK";
                    }else if(i == R.id.radio_keluar){
                        status = "KELUAR";
                    }
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String jumlah = edit_jumlah.getText().toString();
                String keterangan = edit_keterangan.getText().toString();
                // cek validasi
                if(jumlah.equals("") || keterangan.equals("") || status.equals("")){
                    Snackbar.make(view, "Form Tidak Boleh Kosong", Snackbar.LENGTH_LONG).show();
                }else{
                    //debug di logcat
                    Log.d("testing", "Status : "+status+ "jumlah : "+ jumlah + "keterangan : "+ keterangan );

                    // save sqlite
                    SQLiteDatabase database = sqliteHelper.getWritableDatabase();
                    database.execSQL(
                            "INSERT INTO transaksi (status, jumlah, keterangan) VALUES" +
                                    "('"+ status +"','"+ jumlah +"','"+ keterangan +"')"
                    );
                    Toast.makeText(AddActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    finish();
                }

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}