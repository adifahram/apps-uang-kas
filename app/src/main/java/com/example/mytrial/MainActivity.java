package com.example.mytrial;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.mytrial.helper.SqliteHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    // swipe refresh
    SwipeRefreshLayout swipeRefreshLayout;

    // variable sqlite untuk tampil data
    String query_kas;
    ArrayList<HashMap<String, String>> arus_kas = new ArrayList<>();
    SqliteHelper sqliteHelper;
    Cursor cursor;

    // variable untuk list data
    ListView list_kas;

    TextView text_masuk, text_keluar, text_saldo;


    String transaksi_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        //variable swipe_refresh di oncreate
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                KasAdapter();
            }
        });

        //variable list kas di oncreate
        list_kas = (ListView) findViewById(R.id.list_kas);

        text_masuk = (TextView) findViewById(R.id.text_masuk);
        text_keluar = (TextView) findViewById(R.id.text_keluar);
        text_saldo = (TextView) findViewById(R.id.text_saldo);

        //variable query onCreate
        query_kas = "SELECT * FROM transaksi ORDER BY transaksi_id DESC";
        sqliteHelper = new SqliteHelper(this);
        KasAdapter();
        transaksi_id = "";

        // list mennu kalau di klik
        list_kas.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                transaksi_id = ((TextView) view.findViewById(R.id.text_transaksi_id)).getText().toString();
                Listmenu();
            }
        });



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AddActivity.class));

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        KasAdapter();
    }

    private void KasAdapter() {
        swipeRefreshLayout.setRefreshing(false);

        arus_kas.clear(); list_kas.setAdapter(null);

        NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);

        SQLiteDatabase database = sqliteHelper.getReadableDatabase();
        cursor = database.rawQuery(query_kas, null);
        cursor.moveToFirst();
        int totalMasuk = 0;
        int totalKeluar = 0;
        for (int i = 0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);


            if (cursor.getString(1).equals("MASUSK") || cursor.getString(1).equals("MASUK")){
                totalMasuk += cursor.getInt(2);
            }else if (cursor.getString(1).equals("KELUAR")){
                totalKeluar += cursor.getInt(2);
            }

            HashMap<String, String> map = new HashMap<>();
            map.put("transaksi_id", cursor.getString(0));
            map.put("status", cursor.getString(1));
            map.put("jumlah",rupiah.format( cursor.getInt(2)));
            map.put("keterangan", cursor.getString(3));
            map.put("tanggal", cursor.getString(4));

            arus_kas.add(map);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(this, arus_kas, R.layout.list_kas ,
                new String[]{ "transaksi_id", "status", "jumlah","keterangan", "tanggal" },
                new int[]{R.id.text_transaksi_id, R.id.text_status,R.id.text_jumlah, R.id.text_keterangan, R.id.text_tanggal});

        list_kas.setAdapter(simpleAdapter);

        Log.d("totalMasuk", rupiah.format(totalMasuk));
        text_masuk.setText(rupiah.format(totalMasuk));
        text_keluar.setText(rupiah.format(totalKeluar));
        text_saldo.setText(rupiah.format(totalMasuk - totalKeluar));


    }


    private void Listmenu(){
        Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.list_menu);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();

        TextView btn_edit = (TextView)dialog.findViewById(R.id.btn_edit);
        TextView btn_hapus = (TextView)dialog.findViewById(R.id.btn_hapus);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        btn_hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Hapus();
            }
        });

    }


    private void Hapus(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Yakin Hapus?");


        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SQLiteDatabase database = sqliteHelper.getWritableDatabase();
                database.execSQL(
                        "DELETE FROM transaksi WHERE transaksi_id='"+transaksi_id +"'"
                );
                Toast.makeText(MainActivity.this, "DATA BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
                KasAdapter();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

}